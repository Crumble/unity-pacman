![Preview gif](https://i.imgur.com/Tk2ohzU.mp4)

UNITY 2018.1

A super simple basic pacman game to get you started.

Only has pacman movement and 1 ghost that just circles the centre.. You can implement further improve by implementing ghost logic and adding powerups.

I may update this package at a later date with more features.

Use Arrow keys to move

You can test it [here](https://crumble.gitlab.io/unity-pacman)